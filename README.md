Panfrost Non-DRM
==================

This repository implements the glue between the upstream Panfrost userspace and
the legacy, non-DRM kernel driver (`mali_kbase`). This code is licensed under
the GPLv2, like the kernel driver itself, and as such is not distributed with
the upstream driver.
