/*
 * © Copyright 2017-2018 Alyssa Rosenzweig
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>
#include <sys/mman.h>
#include <assert.h>
#include <xf86drm.h>

#include "util/u_format.h"
#include "util/u_memory.h"

#include <panfrost-misc.h>
#include <mali_base_kernel.h>
#include <mali-kbase-ioctl.h>

#include "pan_nondrm.h"
#include "pan_resource.h"
#include "pan_context.h"
#include "pan_swizzle.h"
#include "pan_trace.h"

/* From the kernel module */

#define BASE_MEM_MAP_TRACKING_HANDLE (3ull << 12)

struct panfrost_nondrm {
	struct panfrost_driver base;
	int fd;
};

static int
pandev_ioctl(int fd, unsigned long request, void *args)
{
        return ioctl(fd, request, args);
}

static int
pandev_general_allocate(int fd, int va_pages, int commit_pages,
                        int extent, int flags,
                        u64 *out, int *out_flags)
{
        int ret;
        union kbase_ioctl_mem_alloc args = {
                .in.va_pages = va_pages,
                .in.commit_pages = commit_pages,
                .in.extent = extent,
                .in.flags = flags,
        };

        ret = ioctl(fd, KBASE_IOCTL_MEM_ALLOC, &args);
        if (ret) {
                fprintf(stderr, "panfrost: Failed to allocate memory, va_pages=%d commit_pages=%d extent=%d flags=0x%x rc=%d\n",
                        va_pages, commit_pages, extent, flags, ret);
                abort();
        }
        *out = args.out.gpu_va;
        *out_flags = args.out.flags;

        return 0;
}

static struct panfrost_bo *
panfrost_nondrm_import_bo(struct panfrost_screen *screen, struct winsys_handle *whandle)
{
	struct panfrost_bo *bo = CALLOC_STRUCT(panfrost_bo);
	struct panfrost_nondrm *nondrm = (struct panfrost_nondrm *)screen->driver;
        struct drm_mode_map_dumb map_arg;
        int ret;
        unsigned gem_handle;
        union kbase_ioctl_mem_import framebuffer_import = {
                .in = {
                        .phandle = (uint64_t) (uintptr_t) &whandle->handle,
                        .type = BASE_MEM_IMPORT_TYPE_UMM,
                        .flags = BASE_MEM_PROT_CPU_RD |
                                 BASE_MEM_PROT_CPU_WR |
                                 BASE_MEM_PROT_GPU_RD |
                                 BASE_MEM_PROT_GPU_WR |
                                 BASE_MEM_IMPORT_SHARED,
                }
        };

        ret = pandev_ioctl(nondrm->fd, KBASE_IOCTL_MEM_IMPORT, &framebuffer_import);
        assert(ret == 0);

        bo->gpu = (mali_ptr) (uintptr_t) mmap(NULL, framebuffer_import.out.va_pages * 4096, PROT_READ | PROT_WRITE, MAP_SHARED, nondrm->fd, framebuffer_import.out.gpu_va);

        ret = drmPrimeFDToHandle(screen->ro->kms_fd, whandle->handle, &gem_handle);
        assert(ret >= 0);

        memset(&map_arg, 0, sizeof(map_arg));
        map_arg.handle = gem_handle;

        ret = drmIoctl(screen->ro->kms_fd, DRM_IOCTL_MODE_MAP_DUMB, &map_arg);
        assert(!ret);

        bo->cpu = mmap(NULL, framebuffer_import.out.va_pages * 4096, PROT_READ | PROT_WRITE, MAP_SHARED, screen->ro->kms_fd, map_arg.offset);

        u64 addresses[1];
        addresses[0] = bo->gpu;
        struct kbase_ioctl_sticky_resource_map map = {
                .count = 1,
                .address = (u64) (uintptr_t) addresses,
        };
        ret = pandev_ioctl(nondrm->fd, KBASE_IOCTL_STICKY_RESOURCE_MAP, &map);
        assert(ret == 0);

        bo->imported = true;
        bo->size = framebuffer_import.out.va_pages * 4096;
        
        return bo;
}

static int
panfrost_nondrm_export_bo(struct panfrost_screen *screen, int gem_handle, unsigned int stride, struct winsys_handle *whandle)
{
        //fprintf(stderr, "Unimplemented: %s\n", __func__);
        return FALSE;
}

/* Use to allocate atom numbers for jobs. We probably want to overhaul this in kernel space at some point. */
uint8_t atom_counter = 0;

static uint8_t
allocate_atom()
{
        atom_counter++;

        /* Workaround quirk where atoms must be strictly positive */

        if (atom_counter == 0)
                atom_counter++;

        return atom_counter;
}

static int
panfrost_nondrm_submit_vs_fs_job(struct panfrost_context *ctx, bool has_draws, bool is_scanout)
{
        struct pipe_context *gallium = (struct pipe_context *) ctx;
        struct panfrost_screen *screen = pan_screen(gallium->screen);
	struct panfrost_nondrm *nondrm = (struct panfrost_nondrm *)screen->driver;

        struct pipe_surface *surf = ctx->pipe_framebuffer.cbufs[0];

        base_external_resource framebuffer[] = {
                {.ext_resource = surf ? (((struct panfrost_resource *) surf->texture)->bo->gpu | (BASE_EXT_RES_ACCESS_EXCLUSIVE & LOCAL_PAGE_LSB)) : 0},
        };

        int vt_atom = allocate_atom();

        struct base_jd_atom_v2 atoms[] = {
                {
                        .jc = ctx->set_value_job,
                        .atom_number = vt_atom,
                        .core_req = BASE_JD_REQ_CS | BASE_JD_REQ_T | BASE_JD_REQ_CF | BASE_JD_REQ_COHERENT_GROUP | BASEP_JD_REQ_EVENT_NEVER,
                },
                {
                        .jc = panfrost_fragment_job(ctx),
                        .atom_number = allocate_atom(),
                        .core_req = BASE_JD_REQ_FS,
                },
        };

        if (is_scanout) {
                atoms[1].nr_extres = 1;
                atoms[1].extres_list = (u64) (uintptr_t) framebuffer;
        }

        if (screen->last_fragment_id != -1) {
                atoms[0].pre_dep[0].atom_id = screen->last_fragment_id;
                atoms[0].pre_dep[0].dependency_type = BASE_JD_DEP_TYPE_ORDER;
        }

        if (has_draws) {
                atoms[1].pre_dep[0].atom_id = vt_atom;
                atoms[1].pre_dep[0].dependency_type = BASE_JD_DEP_TYPE_DATA;
        }

        atoms[1].core_req |= is_scanout ? BASE_JD_REQ_EXTERNAL_RESOURCES : BASE_JD_REQ_FS_AFBC;

        /* Copy over core reqs for old kernels */

        for (int i = 0; i < 2; ++i)
                atoms[i].compat_core_req = atoms[i].core_req;

        mali_ptr addr = (mali_ptr) (uintptr_t) (atoms + (has_draws ? 0 : 1));
        unsigned nr_atoms = has_draws ? 2 : 1;
        
        /* Dump memory _before_ submitting so we're not corrupted with actual GPU results */
        pantrace_dump_memory();

        struct kbase_ioctl_job_submit submit = {
                .addr = addr,
                .nr_atoms = nr_atoms,
                .stride = sizeof(struct base_jd_atom_v2),
        };

        if (pandev_ioctl(nondrm->fd, KBASE_IOCTL_JOB_SUBMIT, &submit))
                printf("Error submitting\n");

        /* Trace the job if we're doing that and do a memory dump. We may
         * want to adjust this logic once we're ready to trace FBOs */

        for (unsigned i = (has_draws ? 0 : 1); i < 2; ++i) {
                pantrace_submit_job(atoms[i].jc, atoms[i].core_req, FALSE);
        }

        /* Return fragment ID */
        return atoms[1].atom_number;
}

/* Forces a flush, to make sure everything is consistent.
 * Bad for parallelism. Necessary for glReadPixels etc. Use cautiously.
 */

static void
panfrost_nondrm_force_flush_fragment(struct panfrost_context *ctx, struct pipe_fence_handle **fence)
{
        struct pipe_context *gallium = (struct pipe_context *) ctx;
        struct panfrost_screen *screen = pan_screen(gallium->screen);
	struct panfrost_nondrm *nondrm = (struct panfrost_nondrm *)screen->driver;
        struct base_jd_event_v2 event;
        int ret;

        if (!screen->last_fragment_flushed) {
                do {
                        ret = read(nondrm->fd, &event, sizeof(event));
                        if (ret != sizeof(event)) {
                            fprintf(stderr, "error when reading from mali device: %s\n", strerror(errno));
                            break;
                        }

                        if (event.event_code == BASE_JD_EVENT_JOB_INVALID) {
                            fprintf(stderr, "Job invalid\n");
                            break;
                        }
                } while (event.atom_number != screen->last_fragment_id);

                screen->last_fragment_flushed = true;
        }
}

static void
panfrost_nondrm_allocate_slab(struct panfrost_screen *screen,
		              struct panfrost_memory *mem,
		              size_t pages,
		              bool same_va,
		              int extra_flags,
		              int commit_count,
		              int extent)
{
	struct panfrost_nondrm *nondrm = (struct panfrost_nondrm *)screen->driver;
        int flags = BASE_MEM_PROT_CPU_RD | BASE_MEM_PROT_CPU_WR |
                    BASE_MEM_PROT_GPU_RD | BASE_MEM_PROT_GPU_WR;

        int out_flags;

        if (extra_flags & PAN_ALLOCATE_EXECUTE)
                flags |= BASE_MEM_PROT_GPU_EX;

        if (extra_flags & PAN_ALLOCATE_GROWABLE)
                flags |= BASE_MEM_GROW_ON_GPF;

        if (extra_flags & PAN_ALLOCATE_INVISIBLE)
                flags &= ~(BASE_MEM_PROT_CPU_RD | BASE_MEM_PROT_CPU_WR);

        if (extra_flags & PAN_ALLOCATE_COHERENT_LOCAL)
                flags |= BASE_MEM_COHERENT_LOCAL;

        /* w+x are mutually exclusive */
        if (flags & BASE_MEM_PROT_GPU_EX)
                flags &= ~BASE_MEM_PROT_GPU_WR;

        flags |= BASE_MEM_SAME_VA;

	pandev_general_allocate(nondrm->fd, pages,
				commit_count ? commit_count : pages,
				extent, flags, &mem->gpu, &out_flags);

        mem->size = pages * 4096;

        /* The kernel can return a "cookie", long story short this means we
         * mmap
         */
        if (mem->gpu == 0x41000) {
                int prot = (extra_flags & PAN_ALLOCATE_INVISIBLE) ? PROT_NONE : (PROT_READ | PROT_WRITE);

                if ((mem->cpu = mmap(NULL, mem->size, prot, 1,
                                     nondrm->fd, mem->gpu)) == MAP_FAILED) {
                        perror("mmap");
                        abort();
                }
                mem->gpu = (mali_ptr) (uintptr_t) mem->cpu;
        }

        mem->stack_bottom = 0;

        /* Record the mmap if we're tracing */
        if (!(extra_flags & PAN_ALLOCATE_GROWABLE))
                pantrace_mmap(mem->gpu, mem->cpu, mem->size, NULL);
}


static void
panfrost_nondrm_free_slab(struct panfrost_screen *screen, struct panfrost_memory *mem)
{
        /* For SAME_VA, we simply munmap the GPU */

        if (munmap((void *) (uintptr_t) mem->gpu, mem->size)) {
                perror("munmap");
                abort();
        }
}

static void
panfrost_nondrm_free_imported_bo(struct panfrost_screen *screen, struct panfrost_bo *bo) 
{
	struct panfrost_nondrm *nondrm = (struct panfrost_nondrm *) screen->driver;

        assert(bo->imported);

        u64 addresses[1];
        addresses[0] = bo->gpu;
        struct kbase_ioctl_sticky_resource_map map = {
                .count = 1,
                .address = (u64) (uintptr_t) addresses,
        };
        int ret = pandev_ioctl(nondrm->fd, KBASE_IOCTL_STICKY_RESOURCE_UNMAP, &map);
        assert(ret == 0);
        
        /* ..we also munmap both the Mali and DRM side bits */

        if (munmap((void *) (uintptr_t) bo->gpu, bo->size)) {
                perror("munmap");
                abort();
        }

        if (munmap((void *) (uintptr_t) bo->cpu, bo->size)) {
                perror("munmap");
                abort();
        }
}

static void
panfrost_nondrm_enable_counters(struct panfrost_screen *screen)
{
	struct panfrost_nondrm *nondrm = (struct panfrost_nondrm *) screen->driver;

        struct kbase_ioctl_hwcnt_enable enable_flags = {
                .dump_buffer = screen->perf_counters.gpu,
                .jm_bm = ~0,
                .shader_bm = ~0,
                .tiler_bm = ~0,
                .mmu_l2_bm = ~0
        };

        if (pandev_ioctl(nondrm->fd, KBASE_IOCTL_HWCNT_ENABLE, &enable_flags)) {
                fprintf(stderr, "Error enabling performance counters\n");
                return;
        }
}

static void
panfrost_nondrm_dump_counters(struct panfrost_screen *screen)
{
	struct panfrost_nondrm *nondrm = (struct panfrost_nondrm *) screen->driver;

        if (pandev_ioctl(nondrm->fd, KBASE_IOCTL_HWCNT_DUMP, NULL)) {
                fprintf(stderr, "Error dumping counters\n");
                return;
        }
}

static unsigned
panfrost_nondrm_query_gpu_version(struct panfrost_screen *screen)
{
	struct panfrost_nondrm *nondrm = (struct panfrost_nondrm *) screen->driver;
	struct kbase_ioctl_get_gpuprops get_gpuprops = {0,};
	char buf[1024];
	char *ptr;
	int size;

        get_gpuprops.buffer = (u64) (uintptr_t) buf;
        get_gpuprops.size = sizeof(buf);
        size = pandev_ioctl(nondrm->fd, KBASE_IOCTL_GET_GPUPROPS, &get_gpuprops);
        if (size < 1 || size > get_gpuprops.size) {
                fprintf(stderr, "Error getting GPU props: %m\n");
                return 0x0;
        }

        ptr = buf;
        while (ptr < buf + size) {
                u32 key = *((u32 *)ptr);
                u8 type = key & 3;
                key = key >> 2;

                ptr += 4;

                if (key == KBASE_GPUPROP_PRODUCT_ID)
                        return *((u32 *)ptr);

                switch (type) {
                case KBASE_GPUPROP_VALUE_SIZE_U8:
                        ptr += 1;
                        break;
                case KBASE_GPUPROP_VALUE_SIZE_U16:
                        ptr += 2;
                        break;
                case KBASE_GPUPROP_VALUE_SIZE_U32:
                        ptr += 4;
                        break;
                case KBASE_GPUPROP_VALUE_SIZE_U64:
                        ptr += 8;
                        break;
                default:
                        assert(0);
                }
        }

        return 0x0;
}

static int
panfrost_nondrm_init_context(struct panfrost_context *ctx)
{
        //fprintf(stderr, "Unimplemented: %s\n", __func__);
        return 0;
}

static void
panfrost_nondrm_fence_reference(struct pipe_screen *screen,
                         struct pipe_fence_handle **ptr,
                         struct pipe_fence_handle *fence)
{
        //fprintf(stderr, "Unimplemented: %s\n", __func__);
}

static boolean
panfrost_nondrm_fence_finish(struct pipe_screen *pscreen,
                      struct pipe_context *ctx,
                      struct pipe_fence_handle *fence,
                      uint64_t timeout)
{
        //fprintf(stderr, "Unimplemented: %s\n", __func__);
        return TRUE;
}

struct panfrost_driver *
panfrost_create_nondrm_driver(int fd)
{
	struct panfrost_nondrm *driver = CALLOC_STRUCT(panfrost_nondrm);
        struct kbase_ioctl_version_check version = { .major = 11, .minor = 11 };
        struct kbase_ioctl_set_flags set_flags = {};
        int ret;

	driver->fd = fd;

	driver->base.import_bo = panfrost_nondrm_import_bo;
	driver->base.export_bo = panfrost_nondrm_export_bo;
	driver->base.submit_vs_fs_job = panfrost_nondrm_submit_vs_fs_job;
	driver->base.force_flush_fragment = panfrost_nondrm_force_flush_fragment;
	driver->base.allocate_slab = panfrost_nondrm_allocate_slab;
	driver->base.free_slab = panfrost_nondrm_free_slab;
	driver->base.free_imported_bo = panfrost_nondrm_free_imported_bo;
	driver->base.enable_counters = panfrost_nondrm_enable_counters;
	driver->base.dump_counters = panfrost_nondrm_dump_counters;
	driver->base.query_gpu_version = panfrost_nondrm_query_gpu_version;
	driver->base.init_context = panfrost_nondrm_init_context;
	driver->base.fence_reference = panfrost_nondrm_fence_reference;
	driver->base.fence_finish = panfrost_nondrm_fence_finish;
	driver->base.dump_counters = panfrost_nondrm_dump_counters;

        ret = ioctl(fd, KBASE_IOCTL_VERSION_CHECK, &version);
        if (ret != 0) {
                fprintf(stderr, "Version check failed with %d (reporting UK %d.%d)\n",
                        ret, version.major, version.minor);
                abort();
        }
        printf("panfrost: Using kbase UK version %d.%d, fd %d\n", version.major, version.minor, fd);

        if (mmap(NULL, 4096, PROT_NONE, MAP_SHARED, fd, BASE_MEM_MAP_TRACKING_HANDLE) == MAP_FAILED) {
                perror("mmap");
                abort();
        }
        ret = ioctl(fd, KBASE_IOCTL_SET_FLAGS, &set_flags);
        if (ret != 0) {
                fprintf(stderr, "Setting context flags failed with %d\n", ret);
                abort();
        }

        return &driver->base;
}
